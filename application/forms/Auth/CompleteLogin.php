<?php

/**
 * Representa um formulário de Login completo, i. e., campos de entrada 
 * para o identificardor, senha e captcha. 
 *
 * @author maatao
 */
class Application_Form_Auth_CompleteLogin extends Zend_Form {

    public function init() {
        $this->setMethod('post');

        // Add some CSFR protection (www.frameword.zend.com/manual/en/learning.quickstart.create-form.html)
        //$this->addElement('hash', 'csrf', array('ignore' => true));
        
        $this->addElement(
                'text', 'username', array(
            'label' => 'Usuário:',
            'required' => true,
            'maxlength' => '20',        
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'password', 'password', array(
            'label' => 'Senha:',
            'required' => true,
            'malength' => '20',        
                )
        );

        // Add a captcha
        $this->addElement('captcha', 'captcha', array(
            'label'     => 'Entre com os caracteres abaixo:',
            'required'  => true,
            'captcha'   => array(
                'captcha' => 'Figlet',
                'wordLen' => 5,
                'timeout' => 300
            )
        ));
        
        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'label' => 'Login',
                )
        );
    }

}

