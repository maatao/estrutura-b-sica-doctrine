<?php

/**
 * Representa um formulário de Login simplificador, i. e., apenas os campos 
 * do identificador e senha são apresentados. 
 *
 * @author maatao
 */
class Application_Form_Auth_SimpleLogin extends Zend_Form {

    public function init() {
        $this->setMethod('post');

        // Add some CSFR protection (www.frameword.zend.com/manual/en/learning.quickstart.create-form.html)
        //$this->addElement('hash', 'csrf', array('ignore' => true));
        
        $this->addElement(
                'text', 'username', array(
            'label' => 'Usuário:',
            'required' => true,
            'maxlength' => '20',        
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'password', 'password', array(
            'label' => 'Senha:',
            'required' => true,
            'malength' => '20',        
                )
        );

        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'label' => 'Login',
                )
        );
    }

}

