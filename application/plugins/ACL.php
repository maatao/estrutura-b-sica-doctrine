<?php

use Improver\Utils\LogManager;
use Improver\Utils\ACLImprover;
use Authenticator\Model\BusinessLayer\User;

/**
 * Plugin utilizado para o controle de acesso do sistema. 
 *
 * @author maatao
 */


class Application_Plugin_ACL extends Zend_Controller_Plugin_Abstract{

    /**
     * 
     * Objeto que representa o controle de acesso. 
     * 
     */
    private $acl;
    
    
    public function __construct($acl){
        $this->acl = $acl;
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        parent::preDispatch($request);
        
        /**
         * 
         * Pseudo:
         * 
         * 
         * i) Verifico se existe um usuário autenticado no objeto acl
         * i) a) Se não, considero o role como guest
         * i) b) Se sim: verifico se o papel do usuário permite o acesso
         * i) b) 1) o papel deve ser recuperado do banco de dados (um atributo do usuario)
         * ii) Verifica a permissão do papel ao recurso
         * ii) a) Se permitido: continuo o redirecionamento (não faz nada)
         * ii) b) Se não permitido: redireciona para uma página de erro. 
         * (acesso ao conteúdo restrito)
         * 
         */
        
        // autenticador
        $auth = Zend_Auth::getInstance();
        
        // Verifica se tem uma identidade associada, i. e., o usuário fez o 
        // login. Caso positivo obtem a permissão (role) dela. Caso negativo 
        // considera o role como guest.         
        if(!$auth->hasIdentity()){
            $role = new Zend_Acl_Role('guest');
        }else{
            $identity = $auth->getIdentity();
            $role = $identity['role_name'];
        }

        // Verificando se expirou a sessão
        $session_namespace = Zend_Registry::get('session_namespace');
        
        if(!isset($session_namespace->expiration)){
            $auth->clearIdentity();
            $role = new Zend_Acl_Role('guest');
        }else{
            
        }
        
        
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        
        $front = Zend_Controller_Front::getInstance();
        
        /**
         * Verificando o modulo, controller e action requisitados
         * 1) se não existir o recurso (indicado pelo módulo) apresenta tela de erro
         * 2) se não for permitido redireciona para a tela de login         * 
         * 3) se não existir o role (isto indica um erro)
         */
        
        /*Tentativa de acessar uma área inválida. Redirecionando para o site. */
        if($module != 'site' && $module != 'admin' && $module != 'default'){
            $message = 'Tentativa de logar numa página inválida (controller='.$controller.
                    ', action='.$action.', module='.$module.').';
            
            LogManager::getInstance()->logInfoMessage($message);

            
            $controller = 'index';
            $action = 'index';
            $module = 'site';
            
            $request->setControllerName($controller);
            $request->setModuleName($module);
            $request->setActionName($action);
            
            return;
        }
        
        if(!$this->acl->hasRole($role)){
            // Fazer o tratamento adequado para este caso. 
            $front->getResponse()->setHttpResponseCode(404);
            
            $message = 'Tentativa de logar com um Role inválido (role='.$role.').';
            
            LogManager::getInstance()->logInfoMessage($message);
            
            return;
        }
        
        if(!$this->acl->isAllowed($role, $module)){
            if($controller != 'login'){
                $request->setModuleName($module);
                $request->setControllerName('login');
                $request->setActionName('login');

                $message = 'Tentativa de logar numa área restrita (controller='.$controller.
                        ', action='.$action.', module='.$module.')';

                LogManager::getInstance()->logInfoMessage($message);

                return;            
            }
        }else{        
            
        }
    }
}

