<?php

namespace GuestBook\Model\Doctrine\Entity;

/**
 * 
 * @Table(name="signature")
 * @Entity
 */
class SignatureDoctrine{
    
    /**
     * @var integer $id
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    
    /**
     * @Column(type="string", length="300")
     */
    private $message;
    public function setMessage($message){$this->message=$message;}
    public function getMessage(){return $this->message;}
    
    /**
     * @Column(type="datetime")
     */
    private $creation_date;
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreationDate(){return $this->creation_date;}
    
    /**
     * @ManyToOne(targetEntity="Authenticator\Model\Doctrine\Entity\UserDoctrine")
     * @JoinColumn(name="user_id",referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
    public function setUser($user){$this->user=$user;}
    public function getUser(){return $this->user;}
    
    /**
     * @ManyToOne(targetEntity="GuestBook\Model\Doctrine\Entity\GuestBookDoctrine")
     * @JoinColumn(name="guestbook_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $guestbook;
    public function setGuestBook($guestbook){$this->guestbook=$guestbook;}
    public function getGuestBook(){return $this->guestbook;}
}
