<?php

namespace GuestBook\Model\Doctrine\Entity;


/**
 * 
 * @Table(name="guestbook")
 * @Entity
 */
class GuestBookDoctrine{
    
    /**
     * @var integer $id
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    
    /**
     * @Column(type="string", length="100")
     */
    private $title;
    public function setTitle($title){$this->title=$title;}
    public function getTitle(){return $this->title;}
    
    /**
     * @Column(type="datetime")
     */
    private $creation_date;
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreationDate(){return $this->creation_date;}
}
