<?php

namespace GuestBook\Model\EntityMapper;

use Improver\Model\EntityMapper\InterfaceEntityMapper;
use GuestBook\Model\BusinessLayer\Signature;
use GuestBook\Model\Doctrine\Entity\SignatureDoctrine;
use Improver\Utils\LogManager;
use GuestBook\Model\EntityMapper\GuestBookMapper;
use Authenticator\Model\EntityMapper\UserMapper;

class SignatureMapper implements InterfaceEntityMapper{
    
    private static $instance = null;
    
    private function __construct(){}
    private function __clone(){}
    
    public static function getInstance(){
        if(SignatureMapper::$instance == null){
            SignatureMapper::$instance = new SignatureMapper();
        }
        return SignatureMapper::$instance;
    }
    
    public function businnessEntityToDoctrine($object) {
        if(!($object instanceof Signature)){
         throw new \Zend_Exception('Objeto passado não é uma instancia de GuestBook\Model\BusinessLayer\Signature');
        }
        
       // Converte entre os objetos e retorna uma instancia do objeto do doctrine
       $entity_doctrine = new SignatureDoctrine();
       $entity_doctrine->setId($object->getId());  
       $entity_doctrine->setCreationDate($object->getCreationDate());
       $entity_doctrine->setGuestBook(GuestBookMapper::businnessEntityToDoctrine($object->getGuestBook()));
       $entity_doctrine->setUser(UserMapper::businnessEntityToDoctrine($object->getUser()));
       $entity_doctrine->setMessage($object->getMessage());
       
       return $entity_doctrine;
    }

    public function doctrineEntityToBusiness($object) {
        if(!($object instanceof SignatureDoctrine)){
           throw new \Zend_Exception('Objeto passado não é uma instância de GuestBook\Model\Doctrine\Entity\SignatureDoctrine');
        }
        
        // Converte entre os objetos e retorna uma instancia do objeto de negócio 
        $entity_business = new Signature();
        $entity_business->setId($object->getId());
        $entity_business->setCreationDate($object->getCreationDate());
        $entity_business->setGuestBook(GuestBookMapper::doctrineEntityToBusiness($object->getGuestBook()));
        $entity_business->setUser(UserMapper::doctrineEntityToBusiness($object->getUser()));
        $entity_business->setMessage($object->getMessage());
        
        return $entity_business;
    }
    
}

