<?php

namespace Improver\Utils;

/**
 * Description of GuestBook_ACL
 *
 * @author maatao
 */
class ACLImprover extends \Zend_Acl {
    
    public function __construct(){
               
        // Papeis
        $roleGuest = new \Zend_Acl_Role('guest');
        $roleUser = new \Zend_Acl_Role('user');
        $roleAdmin = new \Zend_Acl_Role('admin');
        $this->addRole($roleGuest);
        $this->addRole($roleUser, $roleGuest);
        $this->addRole($roleAdmin, $roleUser);
        
        
        // Recursos
        $siteResource  = new \Zend_Acl_Resource('site');
        $adminResource = new \Zend_Acl_Resource('admin');
        $authResource = new \Zend_Acl_Resource('auth');
        $defaultResource =  new \Zend_Acl_Resource('default');
        
        $this->add($siteResource);
        $this->add($adminResource);
        $this->add($authResource);
        $this->add($defaultResource);
        
        // Liberando o acesso aos recursos para os papeis existentes
        $this->allow('guest','site');
        $this->allow('admin','admin');
        $this->allow('guest', 'auth');
        $this->allow('guest', 'default');
        
    }
}

