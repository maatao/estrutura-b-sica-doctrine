<?php

namespace Authenticator\Model\Doctrine\Entity;

/**
 * 
 * @Table(name="role")
 * @Entity
 */
class RoleDoctrine{
    
    /**
     * @var integer $id
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    
    /**
     * @Column(type="string", length="100")
     */
    private $name;
    public function setName($name){$this->name=$name;}
    public function getName(){return $this->name;}
    
    /**
     * 
     * @OneToMany(targetEntity="Authenticator\Model\Doctrine\Entity\UserDoctrine", mappedBy="role")
     * 
     */
    private $users;
    public function setUsers($users){$this->users=$users;}
    public function getUsers(){return $this->users;}
}
